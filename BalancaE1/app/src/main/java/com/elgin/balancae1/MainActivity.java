package com.elgin.balancae1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.elgin.e1.Balanca.BalancaE1;

/** @author Gasperin **/
public class MainActivity extends AppCompatActivity {

    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mContext = this.getApplicationContext();

        Button btnConfig = findViewById(R.id.btnConfig);
        btnConfig.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                config();
            }
        });

        Button btnLerPeso = findViewById(R.id.btnLerPeso);
        btnLerPeso.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lerPeso();
            }
        });
    }

    private void config() {

        // MODELOS VALIDOS
        final int DP3005        = 0;
        final int SA110         = 1;
        final int DPSC          = 2;

        // PROTOCOLOS VALIDOS
        final int PROTOCOLO0    = 0;
        final int PROTOCOLO1    = 1;
        final int PROTOCOLO2    = 2;
        final int PROTOCOLO3    = 3;
        final int PROTOCOLO4    = 4;
        final int PROTOCOLO5    = 5;
        final int PROTOCOLO7    = 7;

        int retorno1 = BalancaE1.ConfigurarModeloBalanca(DP3005); // Modelo da balanca utilizada...
        int retorno2 = BalancaE1.ConfigurarProtocoloComunicacao(PROTOCOLO0); // Protocolo da balanca...
        mostrarRetorno(String.format("\nConfigurarModeloBalanca: %d\nConfigurarProtocoloComunicacao: %d", retorno1, retorno2));
    }

    private void lerPeso() {
        int retorno1 = BalancaE1.AbrirSerial(9600, 8, 'n', 1); // Configuracao serial da balanca...
        String retorno2 = BalancaE1.LerPeso(1);
        int retorno3 = BalancaE1.Fechar();
        mostrarRetorno(String.format("\nAbrirSerial: %d\nLerPeso: %s\nFechar: %d", retorno1, retorno2, retorno3));
    }

    private void mostrarRetorno(String retorno) {
        Toast.makeText(mContext, String.format("Retorno: %s", retorno), Toast.LENGTH_SHORT).show();
    }
}
